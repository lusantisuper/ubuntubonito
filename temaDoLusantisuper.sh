#!/bin/bash

#Programa feito pelo lusantisuper!
#Sob licensa MIT

usuario="$USER"


clear
echo "Bem-vindo ao programa do lusantisuper de deixar o computador bonito!"
echo ""
echo "Vamos começar!"
echo ""


echo ""
echo "Adicionado repositorios..."
sleep 3
echo "Por-favor me de a permissao root para que eu possa instalar os programas no seu sistema!"
echo ""
sleep 1

sudo add-apt-repository ppa:noobslab/themes -y
sudo apt update
sudo add-apt-repository -u ppa:snwh/ppa -y


echo ""
echo "Instalando Gnome Tweaks para ser aplicado os temas!"
echo ""
sleep 3

sudo apt install gnome-tweaks -y

#echo ""
#echo "Instalando fish para deixar o terminal mais bonito!"
#echo ""
#sleep 3

#sudo apt install fish -y


#echo ""
#echo "Atualizando o ambiente de trabalho!"
#echo ""
#sleep 3
#sudo apt update
#sudo echo "fish" >> /home/$usuario/.bashrc 


echo ""
echo "Instalando temas no sistema"
sudo apt install arc-theme -y
sudo apt install paper-icon-theme -y


echo ""
echo "Sucesso!!"
sleep 1

echo ""
echo "Aplicando temas no seu ubuntu! :D"
echo ""
sleep 5

gsettings set org.gnome.desktop.interface gtk-theme "Arc-Dark"
gsettings set org.gnome.desktop.wm.preferences theme "Arc-Dark"
gsettings set org.gnome.desktop.interface icon-theme "Paper"
gsettings set  org.gnome.desktop.interface cursor-theme "Paper"

echo ""
echo "FIM! :D"
echo ""
sleep 1
